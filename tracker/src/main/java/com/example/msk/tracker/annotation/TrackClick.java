package com.example.msk.tracker.annotation;

/**
 * Created by Marcel Skotnicki on 1/2/18.
 */

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.CLASS)
@Target(ElementType.METHOD)
public @interface TrackClick {
    // optional Annotation
}