package msk.clicktracker.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import msk.clicktracker.R;

public class Sample3Activity extends AppCompatActivity {

    private static final String TAG = Sample3Activity.class.getSimpleName();

    public static Intent createIntent(Context context) {
        return new Intent(context, Sample3Activity.class);
    }

    // ****************************************************************************************************************************************

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample3);

        findViewById(R.id.sample3_text1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "TextView1 clicked");
            }
        });
        findViewById(R.id.sample3_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Button clicked");
            }
        });
        findViewById(R.id.sample3_image).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Image clicked");
            }
        });
        findViewById(R.id.sample3_text2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "TextView2 clicked");
            }
        });
    }
}
