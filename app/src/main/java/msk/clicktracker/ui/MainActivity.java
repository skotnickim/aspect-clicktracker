package msk.clicktracker.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.msk.tracker.Tracker;

import msk.clicktracker.R;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.sample1_button).setOnClickListener(v -> MainActivity.this.onClick(v));
        findViewById(R.id.sample2_button).setOnClickListener(MainActivity.this::onClick);
        findViewById(R.id.sample3_button).setOnClickListener(MainActivity.this::onClick);
    }

    // ****************************************************************************************************************************************

    //@TrackClick // optional annotation variant to track only specific methods
    private void onClick(View v) {
        switch (v.getId()) {
            case R.id.sample1_button:
                startActivity(Sample1Activity.createIntent(this));
                Log.d(TAG, "Sample1 clicked");
                break;
            case R.id.sample2_button:
                startActivity(Sample2Activity.createIntent(this));
                Log.d(TAG, "Sample2 clicked");
                break;
            case R.id.sample3_button:
            default:
                startActivity(Sample3Activity.createIntent(this));
                Log.d(TAG, "Sample3 clicked");
        }
    }

    // ****************************************************************************************************************************************

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.file:
                Toast.makeText(this, "File clicked", Toast.LENGTH_SHORT).show();
                Tracker.onClick("Tracker was clicked");
                return true;
            case R.id.create_new:
                Toast.makeText(this, "Create new clicked", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.open:
                Toast.makeText(this, "Open clicked", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
