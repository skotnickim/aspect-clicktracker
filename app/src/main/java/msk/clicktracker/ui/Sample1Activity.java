package msk.clicktracker.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import msk.clicktracker.R;

public class Sample1Activity extends AppCompatActivity {

    private static final String TAG = Sample1Activity.class.getSimpleName();

    public static Intent createIntent(Context context) {
        return new Intent(context, Sample1Activity.class);
    }

    // ****************************************************************************************************************************************

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample1);

        findViewById(R.id.sample1_text1).setOnClickListener(v -> Log.d(TAG, "TextView1 clicked"));
        findViewById(R.id.sample1_button).setOnClickListener(v -> Log.d(TAG, "Button clicked"));
        findViewById(R.id.sample1_image).setOnClickListener(v -> Log.d(TAG, "Image clicked"));
        findViewById(R.id.sample1_text2).setOnClickListener(v -> Log.d(TAG, "TextView2 clicked"));
    }
}
