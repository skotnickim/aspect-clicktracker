package com.example.msk.tracker.aspect;

/**
 * Created by Marcel Skotnicki on 1/2/18.
 */

import android.util.Log;
import android.view.View;
import android.widget.TextView;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class TrackClickAspect {

    private static final String TAG = TrackClickAspect.class.getSimpleName();

    @Pointcut("execution(* *.onClick(*)) && args(v)")
    public void onViewClicked(View v) { }

    @Before("onViewClicked(v)")
    public void trackViewClicked(View v) {

        // it is recommended to use `@Before` annotation in order to not duplicate method calls.
        // duplication may occur when user already named after and triggered some method inside method given to execution.
        // e.g. firing custom `activity.this.onClick(v)` method inside native `view.onClick(v)` method

        if (v instanceof TextView) {
            Log.d(TAG, ((TextView) v).getText() + " clicked");
        } else {
            Log.d(TAG, v + " clicked");
        }
    }
}

/*

- AOP Advice Types

-- Based on the execution strategy of advices, they are of following types.

--- Before Advice:
These advices runs before the execution of join point methods.
We can use @Before annotation to mark an advice type as Before advice.

--- After (finally) Advice:
An advice that gets executed after the join point method finishes executing, whether normally or by throwing an exception.
We can create after advice using @After annotation.

--- After Returning Advice:
Sometimes we want advice methods to execute only if the join point method executes normally.
We can use @AfterReturning annotation to mark a method as after returning advice.

--- After Throwing Advice:
This advice gets executed only when join point method throws exception, we can use it to rollback the transaction declaratively.
We use @AfterThrowing annotation for this type of advice.

--- Around Advice:
This is the most important and powerful advice. This advice surrounds the join point method and we can also choose whether to
execute the join point method or not. We can write advice code that gets executed before and after the execution of the join point method.
It is the responsibility of around advice to invoke the join point method and return values if the method is returning something.
We use @Around annotation to create around advice methods.

-- Useful test:
https://stackoverflow.com/questions/18016503/aspectj-around-and-proceed-with-before-after

 */