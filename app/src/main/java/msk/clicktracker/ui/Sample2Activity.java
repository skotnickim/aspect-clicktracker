package msk.clicktracker.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import msk.clicktracker.R;

public class Sample2Activity extends AppCompatActivity {

    private static final String TAG = Sample2Activity.class.getSimpleName();

    public static Intent createIntent(Context context) {
        return new Intent(context, Sample2Activity.class);
    }

    // ****************************************************************************************************************************************

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample2);

        findViewById(R.id.sample2_text1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "TextView1 clicked");
            }
        });
        findViewById(R.id.sample2_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Button clicked");
            }
        });
        findViewById(R.id.sample2_image).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Image clicked");
            }
        });
        findViewById(R.id.sample2_text2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "TextView2 clicked");
            }
        });
    }
}
